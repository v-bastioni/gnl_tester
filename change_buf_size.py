def main():
    from sys import argv, exit
    from re import sub, match
    
    if len(argv) < 3:
        print "Usage: python change_buf_size.py <get_next_line.h path> <new buffer size>"
    fn = argv[1]
    b_sz = argv[2]
    f = open (fn, 'r')
    content = f.read()
    f.close()
    f = open(fn, 'w')
    content = sub(r"(# define BUFF_SIZE )([\d]{1,10})",
                  "\g<1>{}".format(b_sz),
                  content)
    f.write(content)
    f.close()

if __name__ == "__main__":
    main()
