#!/bin/bash

path42="~/42FileChecker/"
pathGNL="~/Desktop/gnl/rendu_gnl/"
n42="42FileChecker"
gnl="get_next_line.h"
log_file="result.log"

norm="\e[0m"
bold="\e[1m"
und="\e[4m"
red="\e[31m"
yel="\e[33m"
blue="\e[34m"

path42="${path42/#\~/$HOME}/moulitest_42projects/get_next_line_tests"
pathGNL="${pathGNL/#\~/$HOME}"

do_things() {
	python change_buf_size.py "$1" "$2"
	l="${yel}${bold}${und}Test with buffer of value $2${norm}\n"
	printf "$l"
	printf "$l">>"$log_file"
	make -C $path42/ 2> /dev/null |
	while read line; do
		line="$(echo $line | grep '>>>>')"
		if [[ "$line" != "" ]]; then
			echo "$line" | grep -E -ie 'result|fail'
			echo "$line">>"$log_file"
		fi
	done
}

printf "">"$log_file"

if [[ ! -f $path42/Makefile ]]; then
	echo "Error in path to $n42."
	exit 1
fi
if [[ ! -f ${pathGNL}${gnl} ]]; then
	echo "Error in path to get_next_line.h"
	exit 1
fi
if [[ "$#" -lt "1" ]]
then
	printf "${bold}${red}Not enough params${norm}\n"
	exit 1
fi
if [[ "$1" == "-r" ]]
then
	if [[ "$#" -ne "3" ]] || [[ ! "$2" =~ ^[0-9]{1,9}$ ]] || [[ ! "$3" =~ ^[0-9]{1,9}$ ]] || [[ "$2" -gt "$3" ]]
	then
		(>&2 printf "${red}${bold}Usage: sh test.sh -r <min> <max>${norm}\n")
	else
		for n in $(seq $2 $3)
		do
			do_things "${pathGNL}${gnl}" "$n"
		done
	fi
else
	for arg in "$@"
	do
		if [[ "$arg" =~ ^[0-9]{1,9}$ ]]
		then
			do_things "${pathGNL}${gnl}" "$arg"
		else
			printf "${red}${bold}$arg - Not a number.${norm}\n"
		fi
	done
fi
[[ "$(cat $log_file | wc -l)" -ge 1 ]] && printf "${blue}Results written in $log_file${norm}\n"
